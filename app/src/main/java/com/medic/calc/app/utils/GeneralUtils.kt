package com.medic.calc.app.utils

import android.app.Activity
import android.support.v7.app.AlertDialog
import com.medic.calc.app.R

object GeneralUtils {

    //REQUEST
    const val REQUEST_SHOW_RESULT = 1000

    //EXTRAS
    const val EXTRA_RESULT = "EXTRA_RESULT"

    fun showGenericDialog(activity: Activity, title: String, message: String) {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton(activity.getString(R.string.btn_accept)) { p0, p1 -> }
        builder.show()
    }
}