package com.medic.calc.app.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.medic.calc.app.BaseActivity
import com.medic.calc.app.R

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        setFullScreen()

        val handler = Handler()
        handler.postDelayed({ goLogin() }, 2000)
    }

    private fun goLogin(){
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}