package com.medic.calc.app.utils

import android.content.Context
import com.medic.calc.app.R
import com.medic.calc.app.models.Calculator
import com.medic.calc.app.models.Result

object DataUtils {

    const val TYPE_HEADER = 1
    const val TYPE_ITEM = 2

    fun getEmergencieCalcs(context : Context?) : MutableList<Calculator> {
        val calculators: MutableList<Calculator> = mutableListOf()
        calculators.add(Calculator("Header 1","", TYPE_HEADER))
        calculators.add(Calculator(context?.getString(R.string.calc_apendicitis_aguda), context?.getString(R.string.method_alvarado),TYPE_ITEM))
        calculators.add(Calculator("Urgencias 2", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.",TYPE_ITEM))
        calculators.add(Calculator("Urgencias 3", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.",TYPE_ITEM))
        calculators.add(Calculator("Header 1","", TYPE_HEADER))
        calculators.add(Calculator("Urgencias 4", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.",TYPE_ITEM))
        calculators.add(Calculator("Urgencias 5", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.",TYPE_ITEM))
        calculators.add(Calculator("Urgencias 6", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.",TYPE_ITEM))
        calculators.add(Calculator("Urgencias 7", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.",TYPE_ITEM))
        calculators.add(Calculator("Urgencias 8", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.",TYPE_ITEM))
        calculators.add(Calculator("Header 1","", TYPE_HEADER))
        calculators.add(Calculator("Urgencias 9", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.",TYPE_ITEM))
        return calculators
    }

    fun getTraumaCalcs() : MutableList<Calculator> {
        val calculators: MutableList<Calculator> = mutableListOf()
        calculators.add(Calculator("Header 1","", TYPE_HEADER))
        calculators.add(Calculator("Trauma 1", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.",TYPE_ITEM))
        calculators.add(Calculator("Trauma 2", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.",TYPE_ITEM))
        calculators.add(Calculator("Trauma 3", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.",TYPE_ITEM))
        calculators.add(Calculator("Header 1","", TYPE_HEADER))
        calculators.add(Calculator("Trauma 4", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.",TYPE_ITEM))

        return calculators
    }

    fun getApendicitisResult(puntuation : Int, context: Context) : Result? {
        var result : Result? = null
        if (puntuation <= 10)
            result = Result(context.getString(R.string.conclusion_1),context.getString(R.string.actuation_1))
        if (puntuation <= 8)
            result =  Result(context.getString(R.string.conclusion_2),context.getString(R.string.actuation_2))
        if (puntuation <= 6)
            result =  Result(context.getString(R.string.conclusion_3),context.getString(R.string.actuation_3))
        if (puntuation <= 4)
            result =  Result(context.getString(R.string.conclusion_4),context.getString(R.string.actuation_3))

        return result
    }
}