package com.medic.calc.app.ui.calcluators.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.medic.calc.app.R
import com.medic.calc.app.models.Calculator
import com.medic.calc.app.utils.DataUtils
import kotlinx.android.synthetic.main.item_calculator.view.*

class CalcAdapter (val calculators: MutableList<Calculator>, val onCalculatorClick: (Calculator) -> Unit) : RecyclerView.Adapter<CalcAdapter.ViewHolderStyle>() {

    override fun onBindViewHolder(holder: ViewHolderStyle, position: Int) {
        holder?.bindStyle(calculators[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderStyle {
        when (viewType) {
            DataUtils.TYPE_HEADER -> return ViewHolderStyle(LayoutInflater.from(parent.context).inflate(R.layout.item_calculator_header, parent, false))
            DataUtils.TYPE_ITEM -> return ViewHolderStyle(LayoutInflater.from(parent.context).inflate(R.layout.item_calculator, parent, false))
        }

        return ViewHolderStyle(LayoutInflater.from(parent.context).inflate(R.layout.item_calculator, parent, false))
    }

    override fun getItemCount(): Int = calculators.size

    inner class ViewHolderStyle(view: View) : RecyclerView.ViewHolder(view) {
        fun bindStyle(calculator: Calculator) {
            with(calculator) {

                itemView.txtName.text = calculator.name

                if (calculator.type == DataUtils.TYPE_ITEM){
                    itemView.txtDescription.text = calculator.description

                    itemView.setOnClickListener {
                        onCalculatorClick(calculator)
                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return calculators[position].type
    }
}