package com.medic.calc.app.models

open class Calculator (

    var name: String? = null,
    var description: String? = null,
    var type: Int = 0

)
