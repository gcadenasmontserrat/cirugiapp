package com.medic.calc.app.ui.calcluators.adapters

import android.os.Parcelable
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.view.ViewGroup

class ViewPagerCalcAdapter(manager: FragmentManager?) : FragmentStatePagerAdapter(manager) {

    private val mFragmentList = mutableListOf<Fragment>()
    private val mFragmentTitleList = mutableListOf<String>()

    override fun getItem(position: Int): Fragment = mFragmentList[position]

    override fun getCount(): Int = mFragmentList.size

    fun addFragment(fragment: Fragment, title: String) {
        mFragmentTitleList.add(title)
        mFragmentList.add(fragment)
    }

    fun removeFragments(){
        mFragmentList.clear()
        mFragmentTitleList.clear()
    }

    override fun saveState(): Parcelable? = null

    override fun getPageTitle(position: Int): CharSequence = mFragmentTitleList[position]

    override fun getItemPosition(`object`: Any): Int = PagerAdapter.POSITION_NONE

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        container.let { super.setPrimaryItem(it, position, `object`) }
    }
}