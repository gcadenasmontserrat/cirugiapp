package com.medic.calc.app

import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import com.medic.calc.app.utils.GeneralUtils

abstract class BaseActivity : AppCompatActivity() {

    fun setFullScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    }

    fun showGenericDialog(title: String, message: String) {
        GeneralUtils.showGenericDialog(this, title, message)
    }
}