package com.medic.calc.app.ui.calcluators.emergencies

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.medic.calc.app.BaseActivity
import com.medic.calc.app.R
import com.medic.calc.app.models.Result
import com.medic.calc.app.ui.calcluators.ResultsActivity
import com.medic.calc.app.utils.DataUtils
import com.medic.calc.app.utils.DataUtils.getApendicitisResult
import com.medic.calc.app.utils.GeneralUtils
import kotlinx.android.synthetic.main.activity_apendicitis_aguda.*


class ApendicitisAgudaActivity : BaseActivity() {

    private var puntuation : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apendicitis_aguda)
        setSupportActionBar(toolbar)

        setupToolbar()
        setupListeners()
    }

    fun setupToolbar(){
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(true)
        actionBar.setDisplayShowTitleEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }

    fun setupListeners(){
        btnResult.setOnClickListener {
            logicValidationValues()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun logicValidationValues(){
        if (editTemperatura.text.isEmpty() || editLeucocitos.text.isEmpty() || editNeutrofilos.text.isEmpty())
            Toast.makeText(this, getString(R.string.error_empty_values), Toast.LENGTH_LONG).show()
        else if (Integer.parseInt(editNeutrofilos.text.toString()) > 100)
            Toast.makeText(this, getString(R.string.error_percentage), Toast.LENGTH_LONG).show()
        else
            logicCalculePuntuation()
    }

    private fun logicCalculePuntuation(){
        puntuation = 0

        if (swAnorexia.isChecked)
            puntuation += 1

        if (swNauseas.isChecked)
            puntuation += 1

        if (swDolor.isChecked)
            puntuation += 1

        if (swDolorFosa.isChecked)
            puntuation += 2

        if (swDolorRebote.isChecked)
            puntuation += 1

        if (editTemperatura.text.toString() > (37.3).toString())
            puntuation += 1

        if (editLeucocitos.text.toString() > (10000).toString())
            puntuation += 2

        if (editNeutrofilos.text.toString() > (70).toString())
            puntuation += 1

        var result = DataUtils.getApendicitisResult(puntuation, this)
        if (result == null)
            Toast.makeText(this, getString(R.string.error_general), Toast.LENGTH_LONG).show()
        else
            goResultsActivity(result)
    }

    private fun goResultsActivity(result : Result){
        val intent = Intent(this, ResultsActivity::class.java)
        intent.putExtra(GeneralUtils.EXTRA_RESULT, result)
        startActivityForResult(intent, GeneralUtils.REQUEST_SHOW_RESULT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            GeneralUtils.REQUEST_SHOW_RESULT -> when (resultCode) {
                RESULT_OK ->  finish()
            }
        }
    }
}