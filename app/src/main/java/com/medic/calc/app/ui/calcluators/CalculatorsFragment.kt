package com.medic.calc.app.ui.calcluators

import android.os.Bundle
import android.view.View
import com.medic.calc.app.BaseFragment
import com.medic.calc.app.R
import com.medic.calc.app.ui.calcluators.adapters.ViewPagerCalcAdapter
import kotlinx.android.synthetic.main.fragment_calculators.*

class CalculatorsFragment : BaseFragment() {

    private var mViewPagerTasksAdapter: ViewPagerCalcAdapter? = null

    private val mEmergenciesFragment: EmergenciesFragment by lazy {
        val bundle = Bundle()
        val fragment = EmergenciesFragment()
        fragment.arguments = bundle
        fragment
    }

    private val mTraumaFragment: TraumaFragment by lazy {
        val bundle = Bundle()
        val fragment = TraumaFragment()
        fragment.arguments = bundle
        fragment
    }

    override val fragmentLayout: Int get() = R.layout.fragment_calculators

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewPager()
    }

    companion object{
        fun newInstance() : CalculatorsFragment{
            return CalculatorsFragment()
        }
    }

    private fun setupViewPager() {
        mViewPagerTasksAdapter = ViewPagerCalcAdapter(fragmentManager)

        mViewPagerTasksAdapter?.addFragment(mEmergenciesFragment, getString(R.string.section_urgencias))
        mViewPagerTasksAdapter?.addFragment(mTraumaFragment, getString(R.string.section_trauma))

        pagerTabs.adapter = mViewPagerTasksAdapter
        tabCalculators.setupWithViewPager(pagerTabs)
    }

}