package com.medic.calc.app.models

import java.io.Serializable

open class Result (

        var conclusion: String? = null,
        var actuation: String? = null

) : Serializable