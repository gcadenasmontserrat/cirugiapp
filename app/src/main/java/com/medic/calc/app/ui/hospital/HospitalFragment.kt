package com.medic.calc.app.ui.hospital

import android.os.Bundle
import android.view.View
import com.medic.calc.app.BaseFragment
import com.medic.calc.app.R

class HospitalFragment : BaseFragment() {

    override val fragmentLayout: Int get() = R.layout.fragment_hospital

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    companion object{
        fun newInstance() : HospitalFragment {
            return HospitalFragment()
        }
    }

}