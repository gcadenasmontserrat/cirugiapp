package com.medic.calc.app.ui.biography

import android.os.Bundle
import android.view.View
import com.medic.calc.app.BaseFragment
import com.medic.calc.app.R

class BiographyFragment : BaseFragment() {

    override val fragmentLayout: Int get() = R.layout.fragment_biography

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    companion object{
        fun newInstance() : BiographyFragment {
            return BiographyFragment()
        }
    }

}