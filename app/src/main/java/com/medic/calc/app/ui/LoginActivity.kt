package com.medic.calc.app.ui

import android.content.Intent
import android.os.Bundle
import com.medic.calc.app.BaseActivity
import com.medic.calc.app.R
import com.medic.calc.app.utils.ConstantsUtils
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setFullScreen()
        setListeners()
    }

    private fun setListeners(){
        btnAccept.setOnClickListener {
            logicOnClickUserPass()
        }

        txtConditions.setOnClickListener {
            goConditionsActivity()
        }
    }

    private fun logicOnClickUserPass() {
        val password = editPassword.text.toString().trim()

        if (password.isNotEmpty() && password.equals(ConstantsUtils.PASSWORD))
            goMain()
        else
            showGenericDialog(getString(R.string.error), getString(R.string.error_credentials))
    }

    private fun goMain(){
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun goConditionsActivity(){
        startActivity(Intent(this, ConditionsActivity::class.java))
    }


}