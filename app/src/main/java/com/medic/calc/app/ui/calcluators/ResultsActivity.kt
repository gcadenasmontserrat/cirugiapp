package com.medic.calc.app.ui.calcluators

import android.app.Activity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.medic.calc.app.BaseActivity
import com.medic.calc.app.R
import com.medic.calc.app.models.Result
import com.medic.calc.app.utils.GeneralUtils
import kotlinx.android.synthetic.main.activity_results.*

class ResultsActivity : BaseActivity() {

    private lateinit var result : Result

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_results)
        setSupportActionBar(toolbar)

        result = intent.getSerializableExtra(GeneralUtils.EXTRA_RESULT) as Result

        setupToolbar()
        loadResult()
        setupListeners()
    }

    fun setupToolbar(){
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(true)
        actionBar.setDisplayShowTitleEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun setupListeners(){
        btnEnd.setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun loadResult(){
        if (result.conclusion != null){
            txtConclusion.visibility = View.VISIBLE
            txtConclusion.text = result.conclusion
        }

        if (result.actuation != null){
            txtActuation.visibility = View.VISIBLE
            txtActuation.text = result.actuation
        }
    }
}