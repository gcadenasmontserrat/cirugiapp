package com.medic.calc.app.ui.calcluators

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.medic.calc.app.BaseFragment
import com.medic.calc.app.R
import com.medic.calc.app.models.Calculator
import com.medic.calc.app.ui.MainActivity
import com.medic.calc.app.ui.calcluators.adapters.CalcAdapter
import com.medic.calc.app.ui.calcluators.emergencies.ApendicitisAgudaActivity
import com.medic.calc.app.utils.DataUtils
import kotlinx.android.synthetic.main.fragment_calculators_list.*


class EmergenciesFragment : BaseFragment(), (Calculator) -> Unit {

    //Variables
    private var calcAdapter: CalcAdapter? = null
    private val calculators: MutableList<Calculator> = mutableListOf()

    override val fragmentLayout: Int get() = R.layout.fragment_calculators_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configRecyclerView()
    }

    private fun configRecyclerView() {
        calculators.addAll(DataUtils.getEmergencieCalcs(context))

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        calcAdapter = CalcAdapter(calculators,this)
        listCalculators.layoutManager = layoutManager
        listCalculators.adapter = calcAdapter
    }

    override fun invoke(calculator: Calculator) {
        when (calculator.name){
            getString(R.string.calc_apendicitis_aguda) -> startActivity(Intent(context, ApendicitisAgudaActivity::class.java))
        }
    }

}