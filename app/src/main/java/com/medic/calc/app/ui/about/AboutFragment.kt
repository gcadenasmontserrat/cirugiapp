package com.medic.calc.app.ui.about

import android.os.Bundle
import android.view.View
import com.medic.calc.app.BaseFragment
import com.medic.calc.app.R

class AboutFragment : BaseFragment() {

    override val fragmentLayout: Int get() = R.layout.fragment_about

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    companion object{
        fun newInstance() : AboutFragment {
            return AboutFragment()
        }
    }

}