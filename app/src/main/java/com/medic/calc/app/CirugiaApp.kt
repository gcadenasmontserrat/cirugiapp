package com.medic.calc.app

import android.support.multidex.MultiDexApplication

class CirugiaApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        //Instance
        instance = this

    }

    companion object {
        lateinit var instance: CirugiaApp
            private set
    }
}