package com.medic.calc.app.ui

import android.os.Bundle
import android.view.MenuItem
import com.medic.calc.app.BaseActivity
import com.medic.calc.app.R
import kotlinx.android.synthetic.main.activity_conditions.*

class ConditionsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_conditions)
        setSupportActionBar(toolbar)

        setupToolbar()
    }

    fun setupToolbar(){
        val actionBar = supportActionBar
        actionBar!!.setDisplayShowHomeEnabled(true)
        actionBar.setDisplayShowTitleEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}