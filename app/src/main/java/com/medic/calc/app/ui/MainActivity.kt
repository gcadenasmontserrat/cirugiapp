package com.medic.calc.app.ui

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import com.medic.calc.app.BaseActivity
import com.medic.calc.app.R
import com.medic.calc.app.ui.about.AboutFragment
import com.medic.calc.app.ui.biography.BiographyFragment
import com.medic.calc.app.ui.calcluators.CalculatorsFragment
import com.medic.calc.app.ui.hospital.HospitalFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        //Check first section
        onNavigationItemSelected(nav_view.menu.getItem(0))
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START))
            drawer_layout.closeDrawer(GravityCompat.START)
        else
            super.onBackPressed()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_calculator -> {
                toolbar.setTitle(R.string.section_calculadoras)
                if (supportFragmentManager.findFragmentById(R.id.content_main) !is CalculatorsFragment)
                    changeFragment(CalculatorsFragment.newInstance())
            }
            R.id.nav_biography -> {
                toolbar.setTitle(R.string.section_biografia)
                if (supportFragmentManager.findFragmentById(R.id.content_main) !is BiographyFragment)
                    changeFragment(BiographyFragment.newInstance())
            }
            R.id.nav_hospital -> {
                toolbar.setTitle(R.string.section_hospital)
                if (supportFragmentManager.findFragmentById(R.id.content_main) !is HospitalFragment)
                    changeFragment(HospitalFragment.newInstance())
            }
            R.id.nav_information -> {
                toolbar.setTitle(R.string.section_sobre)
                if (supportFragmentManager.findFragmentById(R.id.content_main) !is AboutFragment)
                    changeFragment(AboutFragment.newInstance())
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun changeFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.content_main, fragment, fragment.javaClass.simpleName)
        fragmentTransaction.commit()
    }
}
